let fibList = new Map();
const fib = (num) => {
  if(num == 0 )
    return 0;
  if(num == 1 )
    return 1;
  if(fibList.get(num-1) === undefined){
    fibList.set(num-1 , fib(num-1));
  }
  if(fibList.get(num-2) === undefined){
    fibList.set(num-2, fib(num-2));
  }
  fibList.set(num,fibList.get(num-1) + fibList.get(num-2));
  return fibList.get(num);
}

const out = document.getElementById('out');

const input = document.getElementById("in");
let inputState = input.value;
input.addEventListener("input", (e) => {
  e.preventDefault();
  inputState = input.value;
  out.innerText = `The fib of ${inputState} is ${fib(inputState)}`;
});

