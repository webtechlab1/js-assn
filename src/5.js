const out = document.getElementById('out');

const form = document.getElementById('form');
form.onsubmit = (e) => {
  e.preventDefault();
  const val1 = document.getElementById('num1').value;
  const val2 = document.getElementById('num2').value;
  let res = ""
  if(val2 % val1 === 0)
    res = "is"
  else 
    res = "is not"
  out.innerText = `${val2} ${res} a multiple of ${val1}.`
};
