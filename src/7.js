time = document.getElementById('time');
const updateTime = () => {
  time.innerText = new Date().toLocaleString();
  setTimeout(updateTime, 1000);
}
updateTime();
