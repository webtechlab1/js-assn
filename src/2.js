const out = document.getElementById('out');

let in1State = "";
let in2State = "";
const in1 = document.getElementById('num1');
const in2 = document.getElementById('num2');
in1.addEventListener('input', () => {
  in1State = in1.value;
  comp();
});
in2.addEventListener('input', () => {
  in2State = in2.value;
  comp();
});

const comp = () => {
  let res = "";
  if(in1State.length === 0 || in2State.length === 0){
    out.innerText = "";
    return;
  }
  if(in1State === in2State){
    res = "are";
  } else {
    res = "are not";
  }
  out.innerText = `String ${in1State} and ${in2State} ${res} the same.`;
}

