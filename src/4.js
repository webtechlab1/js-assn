const c_area = () => {
  const radius = parseFloat(window.prompt("enter radius"));
  if (radius == NaN) {
    window.alert("Only numerical inputs allowed");
    return;
  }
  window.alert(`Area of circle is ${radius * radius * Math.PI}`);
  document.getElementById('out').innerText = `Area of circle is ${radius * radius * Math.PI}`
  return;
}

c_area();
