const input = document.getElementById('torev');
let num =  input.value || 0;
input.addEventListener('input', (e) => {
  e.preventDefault();
  num = input.value;
});

const hov = document.getElementById('tohov');
const ln = document.getElementById('link');
hov.addEventListener('mouseenter', () => {
  link.setAttribute('href', `reverse://${revNum(num)}`);
});

const revNum = (num) => {
  return String(num).split('').reverse().join('');
};
