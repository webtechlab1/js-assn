sub = (e) => {
  e.preventDefault();
  const val1 = document.getElementById("one").value;
  const val2 = document.getElementById("two").value;
  let ret = 0;
  if (val1 > val2) {
    ret = val1;
  }
  else ret = val2;
  document.getElementById("ans").innerText = ret;
}

document.getElementById("form").onsubmit = sub;
