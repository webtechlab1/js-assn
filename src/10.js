const warning = document.getElementById("warning");

const input = document.getElementById("in");
let inputState = input.value;
input.addEventListener("input", (e) => {
  e.preventDefault();
  if (input.value.length > lim) {
    input.value = inputState;
    warning.innerText = "Input character limit reached"
  }
  else {
    inputState = input.value;
    if(warning.innerText.length != 0 ){
      warning.innerText = "";
    }
  }
});

const limit = document.getElementById("lim");
let lim = limit.value || 200000000
limit.addEventListener("input", (e) => {
  e.preventDefault();
  lim = limit.value;
})

